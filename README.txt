 _______            __   __       ______
|_     _.--.--.----|  |_|  .-----|      | by matthilde
  |   | |  |  |   _|   _|  |  -__|   ---|
  |___| |_____|__| |____|__|_____|______|

TABLE OF CONTENTS
-----------------

 * Introduction
 * Compatibility
 * Files
 * The Interpreter
 * How To Program In TurtleC
   * Integers
   * Arithmetics
   * Control Flow
   * Variable and Function Assignement
   * Array
   * I/O
   * Comments
   * ??????????
   * Examples
 * Purpose?
 * Licensing

INTRODUCTION
------------

TurtleC is a C-like esoteric programming language that is ran by a
little turtle named... well.... Turtle!

It is quite of a slow programming language because Turtle is not very
fast. It also tends to sleep while running the program. So be patient
when running the program!

NOTE: While pretty much I could test in the interpreter has been tested,
      I can't guarantee that the interpreter is 100% stable.

COMPATIBILITY
-------------

The programming language has been entirely developed on a Linux platform
using Python 3.9.5. It does not require any dependencies as it uses the
standard modules included in Python.

NOTE: The programming language has not been tested on Microsoft Windows.

FILES
-----

 - turtlec.py - Core TurtleC features, includes a debug interpreter.
 - trc.py - TrCI, Turtle C Interpreter. The actual interpreter.
 - parser_test.trt - Small script you can't run, designed to test the parser
 - programs/*.trt - Example programs

THE INTERPRETER
---------------

Turtle will introduce itself then proceed to run the program, you will
see in the interface the STDERR and STDOUT. This turtle, as I said in
the introduction, is quite slow and can sleep midway in program
execution.

Usage of both the interpreter and the debug interpreter.

 ./turtlec.py FILENAME [ARGS...]
 ./trc.py FILENAME [ARGS...]

HOW TO PROGRAM IN TURTLEC
-------------------------

If you are familiar with any C-like programming langauge such as C,
C++, JavaScript or Rust (there are plenty so i am not going to list
them all), you will get the grip quite easily.

 Integers
 --------

 The integer is the only type bundled in the programming language.
 Here are how integers looks like:

    noTurtle() = 0
    turtle() = 1
    turtle(turtle()) = 2
	turtle(turtle(turtle(turtle()))) = 4

 Arithmetics
 -----------

 There are builtin functions to do basic arithmetic and logic.

 Woa(a, b) = a + b
  so Woa(turtle(), turtle(turtle())) == turtle(turtle(turtle()))
 WOa(a, b) = a - b
 WoA(a, b) = a * b
 WOA(a, b) = a / b (since these are integers, it does floor division)

 Wow(a, b) = a == b
  Returns 1 if true, else it returns 0
 WoW(a, b) = a < b


 Control Flow
 ------------

 if a>0 then execute the code between brackets
   Mu a { ... }
 if a==0 then execute the code between brackets
   mU a { ... }
 while a>0, execute the code between brackets in loop
   MU a { ... }

 Variable and Function assignement
 ---------------------------------

 Variable assignement goes like this:
  varname = expression;

 NOTE: variable naming has limitations. For example, if your variable
       name does not match this regex: (?i)^t+urtle$
	   the interpreter will crash.
	   THIS APPLIES TO BOTH VARIABLE AND FUNCTION NAMES.

 Variable names are case sensitive.
  ttturtle is a valid variable name
  Turtle is a valid variable name
  brwhiubhwrui is not valid
  TtUrtlE is valid
  turtleeeee is not valid

 Assigning a function is also as easy as doing it in JavaScript.

  mu tTuRtle(tturtle, ttturtle)
  {
     # ...
  }

 The return value of the last instruction ran in the function will be
 what the function returns.

 Array
 -----

 TurtleArray(length) Creates an array of <length> size
 TurtleASet(arr, i, v) arr[i] = v
 TurtleAGet(arr, i) Returns arr[i]
 TurtleALength(arr) Returns the length of an array

 I/O
 ---

 wOa(a, ...) Output integers as ASCII characters
 wOA(a, ...) Output integers as integers
 woA() Reads a byte from input and returns an integer

 TurtleARead(filename)
  Reads from a file, the filename is an array of integers where each
  integer represents an ASCII value to represent a string.

  The function will return an array of integers which is the content
  of the file.

 TurtleArgv is an array of arrays of integers that contains argv.
 
 Comments
 --------

 Like in Python, type a '#' then write your comment. The text after
 # will be ignored.

 ?????????
 ---------

 Truttle1(); ???????????????????????? idk what it does. the source code
             of this function looks kinda obfuscated.

 Examples
 --------

 There are plenty of examples and tests in the programs folder. Even
 thought all features has been tested, I do not guarantee the
 interpreter is stable.

 The programming language is now turing complete since I have added
 arrays. I have wrote a brainfuck interpreter in TurtleC. (I recommend
 to run it using the debug interpreter cus Turtle is very very slow).

PURPOSE?
--------

There aren't really any purpose for this language except being an entry
of Truttle1's turtle-themed esolang jam. However, one of the goals of
this programming language is that programs looks obfuscated at first
glance and must be, well, not difficult to program but to get easily
lost because TURTLES EVERYWHERE

LICENSING
---------

This software is licensed under the WTFPL. see LICENSE for more detail.
