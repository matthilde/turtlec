SAMPLE PROGRAMS IN TURTLEC

This is a folder containing example and test programs that has been written
during TurtleC development or after. Contains a few tests concerning
various features from TurtleC but also programs written for fun such as
a brainfuck interpreter, a deadfish interpreter and an RPN calculator.
