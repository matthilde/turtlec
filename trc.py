#!/usr/bin/env python3
#
# The TurtleC Interpreter by matthilde
#
import os, sys, time, random, math
from turtlec import *

def _clear():
    os.system('cls' if os.name == "nt" else 'clear')

turtle_stderr = ""
turtle_stdout = ""
turtle_pos = 1
file_lines = 0

turtle_quotes = [
    "*yawn* this is tiring, imma sleep for a bit...",
    "o hewwo! i hope you having a nice day :)",
    "i hope i am doing it well....",
    "this program is very interesting...",
    "Achoo! Sowwie >.>"]
ttqq = turtle_quotes.copy()
random.shuffle(ttqq)

def turtle_update():
    global turtle_stderr, ttqq
    _clear()
    n  = random.randint(0, 100)
    w  = 5 if n == 24 else random.randint(5, 30) / 100
    if n == 24:
        if ttqq == []:
            ttqq = turtle_quotes.copy()
            random.shuffle(ttqq)
        turtle_stderr += ttqq.pop() + '\n'
    
    tp = math.floor((turtle_pos / file_lines) * 20)
    print(' '*tp + 'o')
    print('-'*20 + '[GOAL!]\n')

    print("--- STDERR ---")
    stdrr = turtle_stderr.strip()
    if stdrr != '':
        stdrr = stdrr.split('\n')
        print("[Turtle]", "\n[Turtle] ".join(stdrr))

    print("--- STDOUT ---")
    print(turtle_stdout.strip())
    print("--------------")

    time.sleep(w)

def turtle_ppre(a, b, linenum):
    global turtle_pos, turtle_stderr
    if linenum > 0:
        turtle_pos = linenum
    turtle_update()

def turtle_print(*args, end='\n'):
    global turtle_stdout
    turtle_stdout += ' '.join([str(x) for x in args])
    turtle_stdout += end
    turtle_update()

### MODIFIED BUILT-INS

def turtle_wOa(env, *args):
    for x in args:
        x = x(env).value
        turtle_print(chr(x), end='')

def turtle_wOA(env, *args):
    for x in args:
        x = x(env).value
        print(x, end=' ')
    print()

def turtle_woA(env):
    prompt = "[Turtle] Please type a character on your keyboard,\n" + \
             "[Turtle] I need it >.> "
    return TurtleInteger(ord(input(prompt)[0]))

######

def turtle_introduction():
    print(" o   <- the turtle\n---")
    quotes = [
        "Hewwo!",
        "I am Turtle! The small turtle that will run your program!",
        "Be patient with me, I am kinda slow >.<",
        "Know that a function or a while loop can pull me back " + \
        "during my journey >.>"]

    for quote in quotes:
        print("[Turtle]", quote, end=' ')
        input()
    
def main(fn, args):
    global file_lines, turtle_stderr, turtle_pos
    file_lines = len(fn.split('\n'))

    _clear()
    turtle_introduction()

    env = default_env.copy()
    env["wOa"] = turtle_wOa
    env["wOA"] = turtle_wOA
    env["woA"] = turtle_woA
    env["TurtleArgv"] = argv2turtle(args)
    
    try:
        ast = build_ast(tokenize(fn), ppre = turtle_ppre)
        for line in ast:
            line(env)
    except TurtleException as e:
        dl = '\n | '.join(lines[max(e.linenum-3, 0):e.linenum+2])
        turtle_stderr = f"""
Ouchie!!
I have encountered an error that prevents me from continuing at these
lines:
 | {dl}

Error at line {e.linenum}: {e.err}
"""
        turtle_update()
        sys.exit(1)
    except RecursionError:
        turtle_stderr = f"Ouchie! Too much recursion >.<"
        turtle_update()
        sys.exit(1)

    turtle_pos = file_lines
    turtle_stderr += "I finished!!, that was difficult >.<\n"
    turtle_update()
    
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("USAGE: trci FILENAME ...")
        sys.exit(1)
    else:
        try:
            with open(sys.argv[1]) as f:
                main(f.read(), sys.argv[2:])
        except IOError:
            print("Could not open the file")
        except KeyboardInterrupt:
            _clear()
            print("Why are you abandonning me like that ;_;...")
